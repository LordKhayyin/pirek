# Skrypt do ściągania filmu z kamery usb podłączonej kiedyś do raspberry pi 4 nakierowanej na drukarkę 3d.
while :
do
	ssh pi@yuggoth 'v4l2-ctl --set-ctrl=focus_absolute=60'
	sleep 1
	ssh pi@yuggoth 'v4l2-ctl --set-ctrl=focus_absolute=70'
	sleep 1
	ssh pi@yuggoth 'rm out.mp4'
	ssh pi@yuggoth 'v4l2-ctl --set-ctrl=focus_absolute=120'
	ssh pi@yuggoth 'ffmpeg -f video4linux2 -s 640x480 -i /dev/video0 -ss 0:0:2 -frames 500 /home/pi/out.mp4'
	scp pi@yuggoth:out.mp4 .
	mplayer   out.mp4
done
