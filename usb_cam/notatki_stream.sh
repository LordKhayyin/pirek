# Zapis do pliku strumienia z kamery samej
ffmpeg -f v4l2 -framerate 25 -video_size 640x480 -i /dev/video0 test.mp4

#Strumień audio i video do siebie samego:
# http://4youngpadawans.com/stream-camera-video-and-audio-with-ffmpeg/
ffmpeg -f v4l2 -i /dev/video0 -f alsa -i hw:0 -profile:v high -pix_fmt yuvj420p -level:v 4.1 -preset ultrafast -tune zerolatency -vcodec libx264 -r 10 -b:v 512k -s 640x360 -acodec aac -strict -2 -ac 2 -ab 32k -ar 44100 -f mpegts -flush_packets 0 udp://192.168.100.12:5000?pkt_siz

#Strumień audio i video do adresu multicast:
ffmpeg -f v4l2 -i /dev/video0 -f alsa -i hw:0 -profile:v high -pix_fmt yuvj420p -level:v 4.1 -preset ultrafast -tune zerolatency -vcodec libx264 -r 10 -b:v 512k -s 640x360 -acodec aac -strict -2 -ac 2 -ab 32k -ar 44100 -f mpegts -flush_packets 0 udp://239.0.0.1:5000?pkt_siz

# z telefonu należy w vlc wpisać: udp://@239.0.0.1:5000 - koniecznie z @



# Działa mi na raspberry pi z kamerką usb:
ffmpeg -f v4l2 -i /dev/video0 -f alsa -i "plughw:CARD=C525" -profile:v high -pix_fmt yuvj420p -level:v 4.1 -preset ultrafast -tune zerolatency -vcodec libx264 -r 10 -b:v 512k -s 640x360 -acodec aac -strict -2 -ac 2 -ab 32k -ar 44100 -f mpegts -flush_packets 0 udp://239.0.0.1:5000?pkt_siz

#Trzeba było jednak koniecznie sprawdzić nazwę:
v4l2-ctl --list-devices
# mimo, że np dla vlc mikrofon to hw:2,0, to wpisanie tej nazwy nie działało.
