#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Strumieniowanie obrazu z Raspberry Pi Camera za pomocą HTTP.

Autor: Rafał Sarniak

Obsługiwane rozdzielczości:
- 320x240
- 800x600
- 1600x1200
- 1640x1232
- 1920x1080
- 2000x1500
- 2592x1944 (maksymalna rozdzielczość w module OV5647, wymaga 15 fps)

Domyślne ustawienia:
- Rozdzielczość: 2000x1500
- Liczba klatek na sekundę: 24 fps
- Port: 8160

Po uruchomieniu serwera, wideo można oglądać w przeglądarce lub w odtwarzaczu VLC pod wskazanym adresem.
"""

import io
import logging
import socketserver
import threading
import time
import datetime as dt
from http import server
from subprocess import check_output
from socket import gethostname
import picamera
from threading import Condition

# Konfiguracja
PORT = 8160
WIDTH, HEIGHT = 2000, 1500
FPS = 24
RESOLUTION = f"{WIDTH}x{HEIGHT}"

# Strona HTML
PAGE_TEMPLATE = """
<html>
<head>
<title>PIRek</title>
</head>
<body>
<center><h1>PIRek</h1></center>
<center><img src="stream.mjpg" width="{width}" height="{height}"></center>
</body>
</html>
"""

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s [%(levelname)s] %(message)s',
    handlers=[
        logging.FileHandler("app.log"),
        logging.StreamHandler()
    ]
)


class StreamingOutput:
    """Przechowuje i zarządza strumieniowaniem danych wideo."""

    def __init__(self):
        self.frame = None
        self.buffer = io.BytesIO()
        self.condition = Condition()

    def write(self, buf):
        if buf.startswith(b"\xff\xd8"):  # Nowa ramka.
            self.buffer.truncate()
            with self.condition:
                self.frame = self.buffer.getvalue()
                self.condition.notify_all()
            self.buffer.seek(0)
        return self.buffer.write(buf)


class StreamingHandler(server.BaseHTTPRequestHandler):
    """Obsługuje żądania HTTP dla strumienia wideo."""

    def do_GET(self):
        if self.path == "/index.html" or self.path == "/":
            content = PAGE_TEMPLATE.format(width=WIDTH, height=HEIGHT).encode("utf-8")
            self.send_response(200)
            self.send_header("Content-Type", "text/html")
            self.send_header("Content-Length", len(content))
            self.end_headers()
            self.wfile.write(content)
        elif self.path == "/stream.mjpg":
            self._stream_video()
        else:
            self.send_error(404)
            self.end_headers()

    def _stream_video(self):
        """Strumieniuje wideo w formacie MJPG."""
        self.send_response(200)
        self.send_header("Age", 0)
        self.send_header("Cache-Control", "no-cache, private")
        self.send_header("Pragma", "no-cache")
        self.send_header("Content-Type", "multipart/x-mixed-replace; boundary=FRAME")
        self.end_headers()
        try:
            while True:
                with output.condition:
                    output.condition.wait()
                    frame = output.frame
                self.wfile.write(b"--FRAME\r\n")
                self.send_header("Content-Type", "image/jpeg")
                self.send_header("Content-Length", len(frame))
                self.end_headers()
                self.wfile.write(frame)
                self.wfile.write(b"\r\n")
        except Exception as e:
            logging.warning("Removed streaming client %s: %s", self.client_address, str(e))


class StreamingServer(socketserver.ThreadingMixIn, server.HTTPServer):
    allow_reuse_address = True
    daemon_threads = True


def annotate_thread(camera):
    """Wątek dodający znacznik czasu do wideo."""
    while True:
        camera.annotate_text = dt.datetime.now().strftime("%H:%M:%S")
        time.sleep(0.1)


if __name__ == "__main__":
    logging.info("################ Pirek Camera ################ ")
    with picamera.PiCamera(resolution=RESOLUTION, framerate=FPS) as camera:
        logging.info("======== Camera started ========")
        logging.info(f"Typ kamery:{camera.revision}")
        logging.info(f"Wysyłana rozdzielczość: {WIDTH}x{HEIGHT}, {FPS}fps")
        my_ip = check_output(["hostname", "--all-ip-addresses"]).decode().split()[0]
        hostname = gethostname()
        logging.info(f"Adresy:\n - http://{hostname}.local:{PORT}/index.html\n - http://{my_ip}:{PORT}/")

        output = StreamingOutput()
        camera.rotation = 0  # For reverse: 180.
        camera.start_recording(output, format="mjpeg")
        camera.annotate_foreground = picamera.Color("#101010")
        camera.annotate_text_size = 100

        threading.Thread(target=annotate_thread, args=(camera,), daemon=True).start()

        try:
            address = ("", PORT)
            server = StreamingServer(address, StreamingHandler)
            server.serve_forever()
        except KeyboardInterrupt:
            logging.info("Shutting down")
        finally:
            camera.stop_recording()
