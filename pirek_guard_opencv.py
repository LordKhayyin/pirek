"""Strumieniowanie obrazu z Raspberry Pi Camera za pomocą HTTP, oraz detekcja ruchu.
Program wykrywszy ruch, nagrywa krótki fragment i wysyła go na zewnętrzny serwer, oraz wysyła mail z powiadomieniem.

Autor: Rafał Sarniak

Po uruchomieniu serwera, wideo można oglądać w przeglądarce lub w odtwarzaczu VLC pod wskazanym adresem.
"""

import logging
import json
import os
import threading
import time
from http.server import BaseHTTPRequestHandler, HTTPServer
from socket import gethostname
from subprocess import check_output

import cv2

with open("config.json", "r") as config_file:
    config = json.load(config_file)

# Konfiguracja
PORT = config["Camera"]["Port"]
WIDTH = config["Camera"]["Width"]
HEIGHT = config["Camera"]["Height"]
FPS = config["Camera"]["FPS"]
DETECTION_DURATION = config["Guard"]["DetectionDuration"]
REMOTE_PATH = config["Guard"]["RemotePath"]
SERVER_WWW_SITE = config["Guard"]["ServerSite"]
MAIL_OFFSET = config["Guard"]["MailOffset"]
MAIL_RECIPIENT = config["Guard"]["Recipient"]
SSH_USER = config["Ssh"]["User"]
SSH_KEY = config["Ssh"]["Key"]
SSH_ADDRESS = config["Ssh"]["Address"]

LAST_MAIL_TIME = None

# Strona HTML
PAGE_TEMPLATE = """
<html>
<head>
<title>OpenCV Stream</title>
</head>
<body>
<center><h1>OpenCV Stream</h1></center>
<center><img src="stream.mjpg" width="{width}" height="{height}"></center>
</body>
</html>
"""

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("app.log"), logging.StreamHandler()],
)


class VideoStreamHandler(BaseHTTPRequestHandler):
    """Obsługuje żądania HTTP dla strumienia wideo."""

    def do_GET(self):
        if self.path == "/index.html" or self.path == "/":
            content = PAGE_TEMPLATE.format(width=WIDTH, height=HEIGHT).encode("utf-8")
            self.send_response(200)
            self.send_header("Content-Type", "text/html")
            self.send_header("Content-Length", len(content))
            self.end_headers()
            self.wfile.write(content)
        elif self.path == "/stream.mjpg":
            self._stream_video()
        else:
            self.send_error(404)
            self.end_headers()

    def _stream_video(self):
        """Strumieniuje wideo w formacie MJPG."""
        self.send_response(200)
        self.send_header("Age", 0)
        self.send_header("Cache-Control", "no-cache, private")
        self.send_header("Pragma", "no-cache")
        self.send_header("Content-Type", "multipart/x-mixed-replace; boundary=FRAME")
        self.end_headers()
        global output_frame, lock
        try:
            while True:
                with lock:
                    if output_frame is None:
                        continue
                    _, jpeg = cv2.imencode(".jpg", output_frame)
                    frame = jpeg.tobytes()
                self.wfile.write(b"--FRAME\r\n")
                self.send_header("Content-Type", "image/jpeg")
                self.send_header("Content-Length", len(frame))
                self.end_headers()
                self.wfile.write(frame)
                self.wfile.write(b"\r\n")
        except Exception as e:
            print(f"Client disconnected: {e}")


def save_video_thread(cap):
    """Zapisuje wideo do pliku MP4 po wykryciu ruchu."""
    global motion_detected
    fourcc = cv2.VideoWriter_fourcc(*"mp4v")

    while True:
        if motion_detected:
            filename = f"recording_{int(time.time())}.mp4"
            out = cv2.VideoWriter(filename, fourcc, FPS, (WIDTH, HEIGHT))
            logging.debug(f"Rozpoczęto nagrywanie: {filename}")

            start_time = time.time()
            while time.time() - start_time < DETECTION_DURATION:
                ret, frame = cap.read()
                if not ret:
                    break
                out.write(frame)
            out.release()
            logging.debug(f"Zakończono nagrywanie: {filename}")
            send_video_via_scp(filename)
            send_mail_via_ssh(filename)
        time.sleep(1)


def motion_detection_thread(cap):
    """Wykrywa ruch przy użyciu OpenCV."""
    global output_frame, lock, motion_detected
    _, prev_frame = cap.read()
    prev_gray = cv2.cvtColor(prev_frame, cv2.COLOR_BGR2GRAY)

    while True:
        _, frame = cap.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        diff = cv2.absdiff(prev_gray, gray)
        _, thresh = cv2.threshold(diff, 25, 255, cv2.THRESH_BINARY)
        motion = cv2.countNonZero(thresh) > 5000  # Czułość detekcji

        with lock:
            if motion:
                motion_detected = True
                cv2.putText(
                    frame,
                    "Wykryty ruch",
                    (10, 30),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    1,
                    (0, 0, 255),
                    2,
                )
            else:
                motion_detected = False
            output_frame = frame.copy()

        prev_gray = gray
        feed_watchdog()
        time.sleep(1 / FPS)


def feed_watchdog():
    """Feed hardware watchdog.

    sudo apt-get install watchdog
    sudo vim /etc/watchdog.conf
    Zmień:
    watchdog-device = /dev/watchdog
    watchdog-timeout = 15
    interval = 10

    Należy zadbać, by można było pisać do /dev/watchdog.
    sudo vim /etc/udev/rules.d/99-watchdog.rules
    Wpisz:
    KERNEL=="watchdog", MODE="0660", GROUP="pi"
    Potem:
    sudo udevadm control --reload-rules
    ls -l /dev/watchdog

    sudo systemctl start watchdog
    """
    try:
        with open("/dev/watchdog", "w") as wd:
            wd.write("1")
    except IOError as e:
        logging.debug(f"Błąd przy próbie pingowania watchdog: {e}")


def send_video_via_scp(local_file):
    """Wysyła nagranie przez SCP."""
    wynik = os.system(
        f"scp -i {SSH_KEY} {local_file} {SSH_USER}@{SSH_ADDRESS}:./public_html/{REMOTE_PATH} "
    )
    if wynik == 0:
        logging.debug(f"Wysłano plik: {local_file} do {SSH_USER}@{SSH_ADDRESS}")
    else:
        logging.debug(f"Plik nie poszedł. Kod os.system: {wynik}")


def send_mail_via_ssh(filename):
    """Wysyła mail."""
    global LAST_MAIL_TIME
    now = time.time()
    if LAST_MAIL_TIME is not None and now - LAST_MAIL_TIME < MAIL_OFFSET:
        logging.debug("Pomijam wysyłanie maila. Jeszcze za wcześnie.")
        return
    LAST_MAIL_TIME = now
    body = (
        f"Wykryto ruch. Zerknij: {SERVER_WWW_SITE}/~{SSH_USER}/{REMOTE_PATH}{filename}"
    )
    subject = "Alert od Pirek Guard!"
    email_to = MAIL_RECIPIENT
    sendmail_command = f'echo -e "Subject: {subject}\n\n{body}" | sendmail {email_to}'
    wynik = os.system(
        f"ssh -i {SSH_KEY} {SSH_USER}@{SSH_ADDRESS} '{sendmail_command}' "
    )
    if wynik == 0:
        logging.debug(f"Wysłano mail do: {email_to}")
    else:
        logging.debug(f"Mail nie poszedł. Kod os.system: {wynik}")


if __name__ == "__main__":
    logging.info("################ Pirek Guard OpenCV ################ ")
    output_frame = None
    lock = threading.Lock()
    motion_detected = False

    cap = cv2.VideoCapture(0)
    logging.info("======== Camera started ========")
    logging.info(f"Wysyłana rozdzielczość: {WIDTH}x{HEIGHT}, {FPS}fps")
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, WIDTH)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, HEIGHT)

    threading.Thread(target=motion_detection_thread, args=(cap,), daemon=True).start()
    threading.Thread(target=save_video_thread, args=(cap,), daemon=True).start()

    server = HTTPServer(("", PORT), VideoStreamHandler)
    my_ip = check_output(["hostname", "--all-ip-addresses"]).decode().split()[0]
    hostname = gethostname()
    logging.info(
        f"Adresy:\n - http://{hostname}.local:{PORT}/index.html\n - http://{my_ip}:{PORT}/"
    )
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        logging.info("Zamykanie serwera...")
    finally:
        cap.release()
        logging.debug("Kamera wyłączona.")
        server.server_close()
        logging.debug("Serwer zamknięty.")
